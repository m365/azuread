[CmdletBinding()]

param(
    [String] $LogFolder,
    [String] $LogType,
    [int] $WarningAge,
    [int] $ErrorAge
)

if (-not (Test-Path $LogFolder -PathType Container)) {
    return 91
}

$log = Get-ChildItem $LogFolder -Filter "$LogType*"| Sort-Object CreationTime -desc | Select-Object -First 1 
if (-not $log) {
    return 92
}

$ErrorSpan = New-TimeSpan -Hours $ErrorAge
$WarningSpan = New-TimeSpan -Hours $WarningAge

$LogAge = New-TimeSpan -Start $log.CreationTime -End (Get-Date)
if ($LogAge -ge $ErrorSpan) {return 2}
if ($LogAge -ge $WarningSpan) {return 1}

$log = Get-Content $log | ConvertFrom-Json

$log | ForEach-Object {
    if ($_.Type -eq "error") {return 2}
    if ($_.Type -eq "warning") {return 1}
}

return 0