[CmdletBinding()]
param (
    <# CONFIG FILE#>
    [ValidateScript({ Test-Path -PathType Leaf $_ })]
    [string]$ConfigFile
)

$Err = $false;
$Config = Get-Content $ConfigFile -Raw | ConvertFrom-Json

<# REQUIRED #>
# IP
$IP = $Config.IP
if (-not $IP) {
    Write-Host -ForegroundColor Red "IP is required argument!" 
    $Err = $true
} else {
    try {
        [ipaddress]$IP | Out-Null
    }
    catch {
        #NEEDSFIX Neni to idealni, ale nevim jak jinak validovat domain name
        try {
            Test-Connection -TargetName $IP -ErrorAction Stop | Out-Null
        } 
        catch {
            Write-Host -ForegroundColor Red "IP argument is neither valid IP address or domain name that could be resolved!"
            $Err = $true
        }
    }
}

# Port
try {
    # Conversion exception
    $Port = [int]$Config.Port

    if (-not $Port) {
        Write-Host -ForegroundColor Red "Port is required argument!"
        $Err = $true
    } elseif ((0 -gt $Port) -or ($Port -gt 65353)) {
        Write-Host -ForegroundColor Red "Port argument is not within valid range!"
        $Err = $true
    }
} catch {
    Write-Host -ForegroundColor Red "Port needs to be int32!"
    $Err = $True
}

# Type
$Type = $Config.Type
if (-not $Type) {
    Write-Host -ForegroundColor Red "Type is required argument!"
    $Err = $true
} elseif ($Type -notin @("signin", "entraid", "exchange", "sharepoint", "general")) {
    Write-Host -ForegroundColor Red "Type has invalid value!"
    $Err = $true
}

# AppID
$ApplicationId = $Config.ApplicationId
if (-not $ApplicationId) {
    Write-Host -ForegroundColor Red "ApplicationId is required argument!"
    $Err = $true
} else {
    $TestGuid = [System.Guid]::Empty
    if (-not [System.Guid]::TryParse($ApplicationId, [System.Management.Automation.PSReference]$TestGuid)) {
        Write-Host -ForegroundColor Red "ApplicationId is not valid GUID!"
        $Err = $true
    }
}

# TenantID
$TenantId = $Config.TenantId
if (-not $TenantId) {
    Write-Host -ForegroundColor Red "TenantId is required argument!"
    $Err = $true
} else {
    $TestGuid = [System.Guid]::Empty
    if (-not [System.Guid]::TryParse($TenantId, [System.Management.Automation.PSReference]$TestGuid)) {
        Write-Host -ForegroundColor Red "TenantId is not valid GUID!"
        $Err = $true
    }
}

# CertThumbprint
$CertificateThumbprint = $Config.CertificateThumbprint
if (-not $CertificateThumbprint) {
    Write-host -ForegroundColor Red "CertificateThumbprint is required argument!"
    $Err = $true
} else {
    # NEEDFIX Nemam tuseni jak tu parsnout validitu thumprintu
}

<#OPTIONAL#>

if ($Config.DownloadMinutes) { 
    try {
        # Conversion exception
        $DownloadMinutes = [int]$Config.DownloadMinutes
        if ($DownloadMinutes -lt 0) {
            Write-Host -ForegroundColor Red "DownloadMinutes is not positive!"
            $Err = $true
        }
    } catch {
        Write-Host -ForegroundColor Red "DownloadMinutes needs to be positive int32!"
        $Err = $true
    }
}

if ($Config.DownloadOffset) { 
    try {
        # Conversion exception
        $DownloadOffset = [int]$Config.DownloadOffset
        if ($DownloadOffset -lt 0) {
            Write-Host -ForegroundColor Red "DownloadOffset is not positive!"
            $Err = $true
        }
    } catch {
        Write-Host -ForegroundColor Red "DownloadOffset needs to be positive int32!"
        $Err = $true
    }
}

if ($Config.ResendFolder) { 
    $ResendFolder = $Config.ResendFolder 
    if (-not (Test-Path -Path $ResendFolder -PathType Container)) {
        Write-Host -ForegroundColor Red "ResendFolder doesnt exist or is not a folder!"
        $Err = $true
    }
}

if ($Config.LogFile) { 
    $LogFile = $Config.LogFile 
    if (-not (Test-Path -Path $LogFile -PathType leaf)) {
        Write-Host -ForegroundColor Red "LogFile doesnt exist or is not a file!"
        $Err = $true
    }
}

if ($Config.LogFolder) { 
    $LogFolder = $Config.LogFolder 
    if (-not (Test-Path -Path $LogFolder -PathType Container)) {
        Write-Host -ForegroundColor Red "LogFolder doesnt exist or is not a folder!"
        $Err = $true
    }
}

if ($Err) {
    return 1
} else {
    return 0
}