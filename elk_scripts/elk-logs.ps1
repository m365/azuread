#Requires -Version 7.0
<#
.SYNOPSIS
Sends m365 logs to remote server.

.DESCRIPTION
Gets and resends failed sign-in or audit logs from m365 tenant to remote server 
in json format over http. Script gets logs from tenant and attempts to send them, 
if send fails the logs are stored locally. Then next run they are attempted to be 
resend. Script also allows debug input, using file content instead of downloading
logs from tenant.

.PARAMETER ConfigFile
Configuration file containing json of key-value for parameters below. Mainly 
for automatic running of the script.

.PARAMETER IP
IP of remote server to send the logs to.

.PARAMETER Port
Port to send the logs to.

.PARAMETER Type
Type of logs to be downloaded, either azuread signin, entraid (previously audit), 
or o365 exchange, sharepoint, and general.

.PARAMETER DownloadMinutes
Setting of how many minutes back will be downloaded.
With minutes=10 it is <T-10, T)

.PARAMETER DownloadOffset
Setting of how many minutes back we skip
With offset=10 and minutes=10 its <T-20, T-10)

.PARAMETER ResendFolder
Folder where failed sends are stored for resending.

.PARAMETER Interactive
Switch if login to tenant should be interactive (using web browser with login
and password).

.PARAMETER ApplicationId
Application the script should connect through if non-interactive login is used.

.PARAMETER TenantId
Tenant the script should connect to if non-interactive login is used.

.PARAMETER CertificateThumbprint
Certificate thumpring used to authenticate to the application used for
non-interactive logins.

.PARAMETER LogFile
Optional parameter to be used if log file should be specified instead of
generated by the script.

.PARAMETER LogFolder
Optional parameter to be used if folder of log file should be other than the
default one.

.PARAMETER OneLine
Switch if json should be send as oneline instead of human readable form.

.PARAMETER DebugInput
File to be sent instead of downloading new logs from tenant for debug purposes.
#>
[CmdletBinding(DefaultParameterSetName = 'ConfigFileParams')]
param (
    <# CONFIG FILE#>
    [Parameter(Mandatory, ParameterSetName = 'ConfigFileParams', Position = 0)] 
    [Parameter(ParameterSetName = 'SendDebug')]
    [ValidateScript({ Test-Path -PathType Leaf $_ })]
    [string]$ConfigFile,

    <# NON CONFIG FILE - COMMON #>
    [Parameter(Mandatory, ParameterSetName = 'SendInteractive')]
    [Parameter(Mandatory, ParameterSetName = 'SendNonInteractive')]
    [Parameter(ParameterSetName = 'SendDebug')]
    [string]$IP,

    [Parameter(Mandatory, ParameterSetName = 'SendInteractive')]
    [Parameter(Mandatory, ParameterSetName = 'SendNonInteractive')]
    [Parameter(ParameterSetName = 'SendDebug')]
    [ValidateRange(1, 65353)]
    [int]$Port,

    [Parameter(Mandatory, ParameterSetName = 'SendInteractive')]
    [Parameter(Mandatory, ParameterSetName = 'SendNonInteractive')]
    [Parameter(ParameterSetName = 'SendDebug')]
    [ValidateSet("signin", "entraid", "exchange", "sharepoint", "general")]
    [string]$Type,

    <# TIME CONFIG #>
    [Parameter(ParameterSetName = 'SendInteractive')]
    [Parameter(ParameterSetName = 'SendNonInteractive')]
    [ValidateRange(1, [int]::MaxValue)]
    [Int]$DownloadMinutes = 10,

    [Parameter(ParameterSetname = 'SendInteractive')]
    [Parameter(ParameterSetName = 'SendNonInteractive')]
    [ValidateRange(1, [int]::MaxValue)]
    [Int]$DownloadOffset = 10,

    <# RESENDING #>
    [ValidateScript({ Test-Path -PathType Container $_ })]
    [string]$ResendFolder,

    <# INTERACTIVE #>
    [Parameter(ParameterSetName = 'SendInteractive')]
    [Parameter(ParameterSetName = 'SendDebug')]
    [switch]$Interactive,

    <# NON INTERACTIVE #>
    [Parameter(Mandatory, ParameterSetName = 'SendNonInteractive')]
    [Parameter(ParameterSetName = 'SendDebug')]
    [string]$ApplicationId,

    [Parameter(Mandatory, ParameterSetName = 'SendNonInteractive')]
    [Parameter(ParameterSetName = 'SendDebug')]
    [string]$TenantId,

    [Parameter(Mandatory, ParameterSetName = 'SendNonInteractive')]
    [Parameter(ParameterSetName = 'SendDebug')]
    [string]$CertificateThumbprint,

    <# LOG FILE #>
    [Parameter(ParameterSetName = 'SendInteractive')]
    [Parameter(ParameterSetName = 'SendNonInteractive')]
    [Parameter(ParameterSetName = 'SendDebug')]
    [string]$LogFile,

    [Parameter(ParameterSetName = 'SendInteractive')]
    [Parameter(ParameterSetName = 'SendNonInteractive')]
    [Parameter(ParameterSetName = 'SendDebug')]
    [ValidateScript({ Test-Path -PathType Container $_ })]
    [string]$LogFolder = "$PSScriptRoot/logs",

    <# Oneline output #>
    [bool]$OneLine = $true, 

    [Parameter(Mandatory, ParameterSetName = 'SendDebug')]
    [ValidateScript({ Test-Path -PathType Leaf $_ })]
    [string]$DebugInput
)

function Write-ScriptLog { 
    param (
        [string]    $Message,
        [string]    $Type
    )

    $Time = Get-Date -Format "HH:mm:ss"
    Write-Verbose "<$Time> [$Type] $Message"

    if (-not $LogFile) {
        Write-Verbose "No logfile yet!"
        return
    }

    $NewLog = @{
        Time    = $Time
        Type    = $Type
        Message = $Message
    }

    $Global:ScriptLogs += $NewLog
    $Global:ScriptLogs | ConvertTo-Json -AsArray | Out-File $LogFile
}

function Get-AzureLogs {
    param (
        [datetime]      $DateTo = (Get-Date -AsUTC),
        [datetime]      $DateFrom = ($DateTo.AddMinutes(-$DownloadMinutes)),
        [string]        $LogType = $Type
    )

    # Get the correct strings so odata can filter
    $FilterTo = $DateTo.AddMinutes(-$DownloadOffset).ToString("yyyy-MM-ddTHH:mm:00Z")
    $FilterFrom = $DateFrom.Addminutes(-$DownloadOffset).ToString("yyyy-MM-ddTHH:mm:00Z")

    # Get the logs from azure
    try {
        switch ($LogType) {
            'signin' { 
                # ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== #
                # This uses the BETA endpoint for Microsoft Graph, this endpoint    #
                # returns non-interactive sign-ins as well as interactive. Normal   #
                # endpoint returns only interactive. Bevare this is BETA endpoint   #
                # and so can change unexpectedly.                       
                # ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== #
                Write-ScriptLog -Type "info" -Message "Getting sign-in logs for $FilterFrom to $FilterTo"
                # $Logs = Get-MgBetaAuditLogSignIn -Filter "createdDateTime gt $FilterFrom and createdDateTime le $FilterTo" -ErrorAction Stop -All
                # Non beta endpoint
                 $Logs = Get-MgAuditLogSignIn -Filter "createdDateTime gt $FilterFrom and createdDateTime le $FilterTo" -ErrorAction Stop -All
            }
            'audit' {
                Write-ScriptLog -Type "info" -Message "Starting to get audit logs"
                $Logs = Get-MgAuditLogDirectoryAudit -Filter "activityDateTime gt $FilterFrom and activityDateTime le $FilterTo" -ErrorAction Stop -All
            }
            Default {
                Write-ScriptLog -Type "error" -message "How did you get here? - Get-AzureLogs default case"
                exit 1
            }
        }
    } catch {
        Write-ScriptLog -Type "error" -Message "Could not download new $LogType logs!"
        Write-ScriptLog -Type "error" -Message $error[0]

        if (-not (Test-Path ("$PSScriptRoot/Failed"))) {
            try {
                New-Item -ItemType Directory -Path "$PSScriptRoot/Failed" -ErrorAction Stop
                Write-ScriptLog -type "info" -Message "Folder $PSScriptRoot/Failed created"
            } catch {
                Write-ScriptLog -type "error" -Message "New-Item Failed!"
                Write-ScriptLog -type "error" -Message $error[0]
            }
        } else {
            Write-ScriptLog -type "info" -Message "Folder exists"
        }

        $ErrCsvItem = [PSCustomObject]@{
            Type = $LogType
            From = $FilterFrom
            To   = $FilterTo
        }
        $DateStr = $DateFrom.ToString("yyyy-MM-ddTHH-mm-00Z")
        $FileName = "$PSScriptRoot/failed/$($LogType)_$DateStr.txt"
        $ErrCsvItem | ConvertTo-Csv | Out-File -FilePath $FileName
        return $null
    }

    # Info message
    $LogCount = [Int]$Logs.count
    Write-ScriptLog -Type "info" -Message "Got $LogCount logs"

    if ($LogCount -eq 0) {
        Write-ScriptLog -Type "warning" -Message "Got no logs"
        return -1
    }

    return $Logs
}

function Get-FailedLogs {
    Write-ScriptLog -type "info" -message "Getting list of failed downloads"
    $FailedDownloads = Get-ChildItem -Path "$PSScriptRoot/failed/"

    if (($FailedDownloads | Measure-Object).Count -eq 0) {
        Write-ScriptLog -type "info" -message "Nothing failed to download!"
        return
    }

    Write-ScriptLog -type "info" -message "Downloading failed timeslots"
    $FailedDownloads | ForEach-Object {
        $FailedDownloads = Get-Content $_.FullName -Raw | ConvertFrom-Csv
        $Type = $FailedDownloads.Type
        $DateFrom = $FailedDownloads.From
        $DateTo = $FailedDownloads.To
        
        if ($Type -in @("entraid", "signin")) {
            $Logs = Get-AzureLogs -LogType $Type -DateFrom ([datetime]$DateFrom) -DateTo ([datetime]$DateTo)
        } elseif ($Type -in @("exchange", "sharepoint", "general")) {
            $AuthToken = Get-Token
            if ($null -eq $AuthToken) { 
                Write-ScriptLog -type "error" -Message "Could not get auth token!"
                exit 1
            }

            switch ($Type) {
                "exchange" { $Subscription = "Audit.Exchange" }
                "sharepoint" { $Subscription = "Audit.Sharepoint" }
                "general" { $Subscription = "Audit.General" }
            }

            $status = Start-Subscription -Subscriptions $Subscription -AuthToken $AuthToken
            if ($null -eq $status) {
                exit 1
            }

            Get-Status -AuthToken $AuthToken
            $Logs = Get-UnifiedLogs -Subscription $Subscription -AuthToken $AuthToken
        } else {
            return
        }

        if ($null -ne $Logs) {
            $ret = Send-Logs -Logs $Logs
        }
        if ($Logs -eq -1 -or $ret -eq 0) {
            Write-ScriptLog -Type "info" -message "Logs sent - deleting $($_.fullname)"
            Remove-Item $_.Fullname
        }
    }
}

function Send-FileLogs {
    Param (
        [string]    $LogFolder
    )

    # Load the filenames in folder
    Write-ScriptLog -type "info" -message "Getting list of log files"
    $LogFiles = Get-ChildItem -Path $LogFolder

    # Check if we have any
    if (($LogFiles | Measure-Object).Count -eq 0) {
        Write-ScriptLog -type "info" -message "Nothing to resend"
        return
    }

    # Go one by one and try to send them
    Write-ScriptLog -type "info" -message "Sending logs file by file"
    $LogFiles | ForEach-Object {
        $Logs = Get-Content $_.fullname -Raw | ConvertFrom-Json
        $retVal = Send-Logs -Logs $logs
        if (-not $retVal) {
            # If retval is 0, send succesfully, we delete the file
            Write-ScriptLog -type "info" -message "Deleting file $($_.fullname)"
            Remove-Item -Path $_.fullname
        }
    }
}

function Send-Logs {
    Param (
        [pscustomobject]    $Logs
    )

    Write-ScriptLog -type "info" -Message "Sending logs to logstash"

    # Create some variable beforehand
    $UriBuilder = New-Object System.UriBuilder('https', $Ip, $Port, $Type)
    $Uri = $UriBuilder.ToString()
    $SendErr = $False

    # Check if the URI is valid
    if (-not [Uri]::IsWellFormedUriString($Uri, 'Absolute')) {
        Write-ScriptLog -type "error" -Message "Uri not valid! ($Uri)"
        throw "Invalid Uri!"
    }

    if ($OneLine) {
        $Logs = $Logs | ConvertTo-Json -Compress -Depth 10
    } else {
        $Logs = $Logs | ConvertTo-Json -Depth 10
    }

    try {
        # Logstash has by default 100MB filesize limit
        # It cant be set higher, but then I would be caucious of Out Of Memory errors
        $res = Invoke-RestMethod -Method 'Put' -ContentType 'application/json' -Uri $Uri -Body $Logs -StatusCodeVariable 'scv' -MaximumRetryCount 3
        Write-ScriptLog -Type "info" -Message "Received response: $res"
    } catch {
        # If the command fails, display output and set err to true
        $SendErr = $True
        Write-ScriptLog -type "error" -Message "Failed to send!"
        Write-ScriptLog -type "error" -Message $error[0]
    }

    # If I get anything but OK I also set err to true
    if ($scv -and $scv -ne 200) {
        $SendErr = $True
        Write-ScriptLog -type "error" -Message "Non OK response code!"
        Write-ScriptLog -type "error" -Message "Status code: $scv"
    }

    # If for some reason it didnt go through correctly I save the logs to file.
    # Unless I am sending already existing logs - already in json format - then I dont store them
    if ($SendErr -and -not $Json_format) {
        $OutFile = Get-Date -Format "yyyyMMddTHH0000"

        Write-ScriptLog -type "info" -Message "Saving log file $OutFile.json"
        $Logs | ConvertTo-Json -Depth 4 -Compress | Out-File "$PSScriptRoot/$($Type)_logs/$OutFile.json" -Encoding utf8

        Write-ScriptLog -type "info" -Message "Logs written to $OutFile.json"
        return 1
    } elseif ($SendErr) {
        Write-ScriptLog -type "info" -Message "Logs not written to file - resending already stored logs"
        return 1
    } else {
        Write-ScriptLog -type "info" -Message "Logs successfully sent"
        return 0
    }
}

function Get-Token {
    Write-ScriptLog -Type "info" -Message "Getting token"
    try {
        # FIXME - nacitani certu, na muj vkus trosku moc hardcoded
        $Cert = Get-Item Cert:\CurrentUser\My\$CertificateThumbprint

        $TokenSplat = @{
            ClientId          = $ApplicationId
            TenantID          = $TenantId
            ClientCertificate = $Cert
            Scopes            = "$Domain/.default"
        }
        $oauth = Get-MsalToken @TokenSplat
        $token = @{'Authorization' = "$($oauth.TokenType) $($oauth.AccessToken)" }
        Write-ScriptLog -Type "info" -Message "Got token"
        return $token
    } catch {
        Write-ScriptLog -Type "error" -Message "Error in Get-Token"
        Write-ScriptLog -Type "error" -Message $Error[0]
        return $null
    }
}

function Start-Subscription {
    param (
        [string]    $Subscription,
        [hashtable] $AuthToken
    )

    Write-ScriptLog -Type "info" -Message "Starting subscription to $Subscription"

    $Splat = @{
        Method      = "Post"
        Headers     = $AuthToken
        Uri         = "$BaseURI/start?contentType=$Subscription"
        ErrorAction = "Stop"
    }

    try {
        $response = Invoke-WebRequest @Splat
        Write-ScriptLog -Type "info" -Message "Subscription to $Subscription created"
        return $response
    } catch {
        if ($_.Exception.Message -like "The subscription is already enabled*") {
            Write-ScriptLog -Type "info" -Message "Subscription already exists."
            return 0
        } else {
            Write-ScriptLog -Type "error" -Message "Start-Subscription failed!"
            Write-ScriptLog -Type "error" -Message $Error[0]
            return $null
        }
    }

    return 0
}

function Get-Status {
    param (
        [hashtable] $AuthToken
    )
    Write-ScriptLog -Type "info" -Message "Getting subscription status"

    $Subscriptions = Invoke-WebRequest -Headers $AuthToken -Uri "$BaseURI/list"
    $SubsContent = $Subscriptions.Content | ConvertFrom-Json
    $SubsContent | ForEach-Object {
        Write-ScriptLog -Type "info" -Message "$($_.contentType): $($_.status)"
    }
}

function Get-UnifiedLogs {
    param (
        [string]        $Subscription,
        [hashtable]     $AuthToken,
        [datetime]      $DateTo = (Get-Date -AsUTC),
        [datetime]      $DateFrom = ($DateTo.AddMinutes(-$DownloadMinutes))
    )
    Write-ScriptLog -Type "info" -Message "Getting $Subscription"

    $FilterTo = $DateTo.AddMinutes(-$DownloadOffset).ToString("yyyy-MM-ddTHH:mm")
    $FilterFrom = $DateFrom.AddMinutes(-$DownloadOffset).ToString("yyyy-MM-ddTHH:mm")

    $Splat = @{
        Method      = "Get"
        Headers     = $AuthToken
        Uri         = "$BaseUri/content?contentType=$Subscription&PublisherIdentifier=$TenantId&StartTime=$FilterFrom&EndTime=$FilterTo"
        ErrorAction = "Stop"
    }

    try {
        $log = Invoke-WebRequest @Splat
    } catch {
        Write-ScriptLog -Type "error" -Message "Getting pages in Get-UnifiedLogs failed"
        Write-ScriptLog -Type "error" -Message $Error[0]
        return $null
    }

    $ContentPages += $Log

    $NextContentPageUri = $Log.Headers.NextPageUri
    while ($NextContentPageUri) {
        $CurrentContentPage = Invoke-WebRequest -Headers $AuthToken -Uri $NextContentPageUri
        $ContentPages += $CurrentContentPage
        $NextContentPageUri = $CurrentContentPage.Headers.NextPageUri
    }

    Write-ScriptLog -Type "info" -Message "Got all content pages"

    if ($ContentPages.Content.Length -le 2) {
        Write-ScriptLog -Type "warning" -Message "No content pages"
        return $null
    }

    $Uris = @()
    $Pages = $ContentPages.Content.Split(",")

    foreach ($Page in $Pages) {
        if ($page -match "contenturi") {
            $Uri = $Page.Split(":")[2] -replace """"
            $Uri = "https:$Uri"
            $Uris += $Uri
        }
    }

    foreach ($Uri in $Uris) {
        try {
            $LogData += Invoke-RestMethod -Uri $Uri -Headers $AuthToken -Method Get
        } catch {
            Write-ScriptLog -Type "error" -Message "Getting logs in Get-UnifiedLogs failed"
            Write-ScriptLog -Type "error" -Message $Error[0]
            return $null
        }
    }

    return $LogData
}

function Get-Logs {

}

<# "MAIN" #>
If ($ConfigFile) {
    $Config = Get-Content $ConfigFile -Raw | ConvertFrom-Json

    $IP = $Config.IP
    $Port = $Config.Port
    if ($Config.Type) { $Type = $Config.Type }
    if ($Config.DownloadMinutes) { $DownloadMinutes = $Config.DownloadMinutes }
    if ($Config.DownloadOffset) { $DownloadOffset = $Config.DownloadOffset}
    if ($Config.ResendFolder) { $ResendFolder = $Config.ResendFolder }
    $ApplicationId = $Config.ApplicationId
    $TenantId = $Config.TenantId
    $CertificateThumbprint = $Config.CertificateThumbprint
    if ($Config.LogFile) { $LogFile = $Config.LogFile }
    if ($Config.LogFolder) { $LogFolder = $Config.LogFolder }
} 

$global:ScriptLogs = @()
$Domain = "https://manage.office.com"
$BaseURI = "$Domain/api/v1.0/$TenantID/activity/feed/subscriptions"

# Checking if log folder exists and creating if not
if (-not (Test-Path ("$LogFolder"))) {
    try {
        New-Item -ItemType Directory -Path "$LogFolder" -ErrorAction Stop
        Write-ScriptLog -type "info" -Message "Folder $LogFolder created"
    } catch {
        Write-ScriptLog -type "error" -Message "New-Item Failed!"
        Write-ScriptLog -type "error" -Message $error[0]
    }
} else {
    Write-ScriptLog -type "info" -Message "Folder exists"
}


# Setting the log file name
if (-not $LogFile) {
    $Date = Get-Date -Format "yyyy-MM-dd'T'HH-mm-ss"
    $LogFile = "$LogFolder/$($Type)_$($Date).log"
   
    # Should not happen, but just to be sure
    $i = 1
    while (Test-Path "$LogFile" -PathType Leaf) {
        $LogFile = "$LogFolder/log_$Date-$i.log"
        $i += 1
    }

    # So the file exists
    Add-Content -Path $LogFile -Value "[]"
}

Write-ScriptLog -Type "info" -Message "elk-logs started"

if ($DebugInput) {
    $Logs = Get-Content $DebugInput | ConvertFrom-Json
    Write-ScriptLog -Type "info" -Message "Using debug input"
} else {
    if ($Type -in @("exchange", "sharepoint", "general")) {
        # unified
        $AuthToken = Get-Token
        if ($null -eq $AuthToken) { 
            Write-ScriptLog -type "error" -Message "Could not get auth token!"
            exit 1
        }

        switch ($Type) {
            "exchange" { $Subscription = "Audit.Exchange" }
            "sharepoint" { $Subscription = "Audit.Sharepoint" }
            "general" { $Subscription = "Audit.General" }
        }

        $status = Start-Subscription -Subscriptions $Subscription -AuthToken $AuthToken
        if ($null -eq $status) {
            exit 1
        }

        Get-Status -AuthToken $AuthToken
        $Logs = Get-UnifiedLogs -Subscription $Subscription -AuthToken $AuthToken
    } else {
        # If I am getting new ones, I need to check connection and then do things
        Write-ScriptLog -Type "info" -Message "Trying to connect to tenant"
        try {
            if (-not $Interactive) {
                Connect-MgGraph -ClientId $ApplicationId -TenantId $TenantId -CertificateThumbprint $CertificateThumbprint | Out-Null
            } else {
                Connect-MgGraph
            }
        } catch {
            Write-ScriptLog -type "error" -Message "Could not connect to tenant"
            Write-ScriptLog -type "error" -Message $error[0]
            exit 1
        }

        Write-ScriptLog -type "info" -Message "Connected"
        $Logs = Get-AzureLogs
    }
}

if ($null -ne $Logs) {
    try {
        Send-Logs -Logs $Logs
    } catch {
        Write-ScriptLog -Type "error" -Message "Failed to send-logs!"
        Write-ScriptLog -Type "error" -Message $Error[0]
        exit 1
    }
}

# Resend any logs that got stuck on my side, if we got resend folder
if ($ResendFolder) {
    Send-FileLogs -LogFolder $ResendFolder
}

# Get failed logs and try to resend them
Get-FailedLogs

# Delete logs 
Get-ChildItem $LogFolder | Where-Object {$_.LastWriteTime -lt (Get-Date).AddDays(-30)} | Remove-Item

Write-ScriptLog -Type "info" -Message "Script finished succesfully"