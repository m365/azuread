# o365-logstash
Set of things required to pull logs from o365 and sending them to remote server.

## Requirements
For script:
- Powershell 7.0 or higher and Microsoft.Graph module
- For interactive login one of these Azure AD roles:
    - Report Reader
    - Security Reader
    - Security Administrator
    - Global Reader
- For non-interactive logins:
    - Application with AuditLog.Read.All Permission  

## Script
`.\elk-logs.ps1 -ConfigFile CONFIG_FILE`
`.\elk-logs.ps1 -IP IP -Port PORT -Type TYPE -Interactive [-DownloadMinutes MINUTES] [-DownloadOffset OFFSET][-ResendFolder RESEND_FOLDER] [-OneLine]`
`.\elk-logs.ps1 -IP IP -Port PORT -Type TYPE -ApplicationId APP_ID -TenantId TENANT_ID -CertificateThumbprint CERT [-DownloadMinutes MINUTES] [-DownloadOffset OFFSET] [-ResendFolder RESEND_FOLDER] [-OneLine]`
`.\elk-logs.ps1 -IP IP -Port PORT -Type TYPE -DebugInput DEBUG_FILE [-ResendFolder RESEND_FOLDER] [-OneLine]`

Where:
- `CONFIG_FILE` is a json file containing key-value pairs for wanted settings, useful in automatic script execution.

- `IP` is address of remote server, e.g. `124.52.38.95` (IPv6 also works in []), or domain name, e.g. `logs.myservice.com`
- `PORT` is port of the remote enpoint, e.g. `8080`
- `TYPE` is type of logs you are sending (and downloading), either `singin`, `entraid`, `exchange`, `sharepoint` or `general`.

`-Interactive` switch makes the script use interactive login.
It will open up login page in web browser.
Beware, each graph api call will need another autorization (needs fix).
Also, if you close the authorization tab, the powershell will wait indefinitely for response, freezing it in process.

Without interactive switch, you need to supply these parameters:
- `APP_ID` application ID of application you want to use to download logs
- `TENANT_ID` tenant ID you want to download the logs from
- `CERT` certificate you have added to the application 

Specifiying from which timeframe you want to download can be done using `MINUTES` and `OFFSET`.
Where what you download is `<T-OFFSET-MINUTES, T-MINUTES>`.
With both set by default to `10` its `<T-20, T-10)`

`RESEND FOLDER` parameter sets which folder the script should look into for logs that failed to be sent.
`-OneLine` flag sets if the output should be oneline with separators (default) or in human readable form.


## ELK
One of the usual log storage solutions is Elk stack.
Elk stack can be easily run with included `docker-compose.yml`.
Contents are taken and edited to fit current usecase from [elastic blog](https://www.elastic.co/blog/getting-started-with-the-elastic-stack-and-docker-compose).
It can be also found on [github](https://github.com/elkninja/elastic-stack-docker-part-one) under the Apache-2.0 license.
### Setup
First, we need to get the container going
`docker compose up`
For logstash->elasticsearch communication we need to copy certs
`docker compose cp es01:/usr/share/elasticsearch/config/certs ./certs`

Kibana is running on `http://localhost:5601`
Elastic is running on `https://localhost:9200`
Username: `elastic` 
Password (configurable in .env) `changeme`

Logstash inputs are configured in `logstash.conf`
By default, logstash expects sign in logs at port 8080 and audit at 8081.

### Data input
You can either input data using the script or using `curl -X PUT ...`
Config is made to handle json data.

### HTTPS logstash
Logstash is setup to use https connections using the certificate found in `snakeoil` directory. 
For production use you should generate new certificate and private key.
Sadly, I could not find a way to generate this using powershell cmdlets.
But here is a way to generate new key and certificate using openssl:

`openssl req -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out snakeoil.crt -keyout snakeoil.key -subj /CN=snakeoil/`